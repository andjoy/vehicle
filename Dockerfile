FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /apps
COPY requirements.txt /apps/
RUN pip install -r requirements.txt
COPY . /apps/

RUN python manage.py migrate

CMD python manage.py runserver 0.0.0.0:8000
