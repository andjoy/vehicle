from django.db import models

class Vehicle(models.Model):
    manufacturer = models.CharField(max_length=60)
    version = models.CharField(max_length=255)
    type = models.CharField(max_length=60)
    license_plate = models.CharField(max_length=60)

    def __str__(self) -> str:
        return self.manufacturer