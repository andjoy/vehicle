from django.db import models
from rest_framework import serializers
from rest_framework import serializers
from .models import Vehicle

class VehicleSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Vehicle
        fields = ('manufacturer', 'type', 'version', 'license_plate')