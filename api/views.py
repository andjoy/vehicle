from rest_framework import viewsets
from .serializers import VehicleSerializers
from .models import Vehicle

class VehicleViewSet(viewsets.ModelViewSet):
    queryset = Vehicle.objects.all().order_by('id')
    serializer_class = VehicleSerializers